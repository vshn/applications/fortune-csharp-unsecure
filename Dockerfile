# Stage 1: builder image
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /build
COPY fortune-dotnet.csproj .
RUN dotnet restore
COPY . .
RUN dotnet publish --configuration release --output ./out --no-restore

# Stage 2: runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine
RUN apk add fortune
WORKDIR /dotnetapp
COPY --from=build /build/out .
USER 1001
EXPOSE 8080
ENTRYPOINT ["dotnet", "fortune-dotnet.dll"]
