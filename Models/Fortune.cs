using System.ComponentModel.DataAnnotations;

namespace fortune_dotnet.Models;

public class Fortune
{
    [Required]
    [StringLength(15, MinimumLength = 3)]
    public string? Name { get; set; }

    [Required]
    public int Number { get; set; }

    [Required]
    public string? Message { get; set; }

    [Required]
    public string? Version { get; set; }

    [Required]
    public string? Hostname { get; set; }

    public override string ToString() => $@"Fortune {Version} cookie of the day #{Number} for {Name}:

{Message}
Pod: {Hostname}";
}
